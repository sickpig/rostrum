use crate::chaindef::BlockHash;
use crate::errors::*;
use bitcoincash::hashes::hex::{FromHex, ToHex};
use bitcoincash::hashes::Hash;
use serde_json::Value;

pub fn hex_to_txid<T: Hash>(value: &Value) -> Result<T> {
    T::from_hex(
        value
            .as_str()
            .chain_err(|| format!("non-string txid value: {}", value))?,
    )
    .chain_err(|| format!("non-hex txid value: {}", value))
}

pub fn txid_to_hex<T: Hash>(value: &T) -> String {
    value.to_hex()
}

/**
 * Bitcoind format
 */
#[cfg(not(feature = "nexa"))]
pub fn hex_to_blockhash(value: &Value) -> Result<BlockHash> {
    BlockHash::from_hex(
        value
            .as_str()
            .chain_err(|| format!("non-string blockhash value: {}", value))?,
    )
    .chain_err(|| format!("non-hex blockhash value: {}", value))
}

/**
 * Nexa RPC uses little endian for block hash
 */
#[cfg(feature = "nexa")]
pub fn hex_to_blockhash(value: &Value) -> Result<BlockHash> {
    let mut hash = hex::decode(
        value
            .as_str()
            .chain_err(|| format!("non-string blockhash value: {}", value))?,
    )
    .chain_err(|| format!("non-hex blockhash value: {}", value))?;
    hash.reverse();
    BlockHash::from_slice(&hash).chain_err(|| format!("invalid blockhash {}", value))
}

#[cfg(not(feature = "nexa"))]
pub fn blockhash_to_hex(value: &BlockHash) -> String {
    value.to_hex()
}

#[cfg(feature = "nexa")]
pub fn blockhash_to_hex(value: &BlockHash) -> String {
    let mut h = value.to_vec();
    h.reverse();
    hex::encode(h)
}
