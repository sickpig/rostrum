use crate::mempool::ConfirmationState;
use crate::nexa::hash_types::OutPointHash;
use bitcoincash::hash_types::Txid;

pub struct FundingOutput {
    pub txid: Txid,
    pub vout: u32,
    pub outpoint_hash: OutPointHash,
    pub height: u32,
    pub value: u64,
    pub state: ConfirmationState,
}

pub struct SpendingInput {
    pub txn_id: Txid,
    pub height: u32,
    pub funding_output_hash: OutPointHash,
    pub value: u64,
    pub state: ConfirmationState,
}
