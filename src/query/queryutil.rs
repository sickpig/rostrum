use crate::chaindef::OutPoint;
use crate::chaindef::Transaction;
use crate::encode::output_hash;
use crate::errors::*;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::mempool::{ConfirmationState, Tracker, MEMPOOL_HEIGHT};
use crate::query::primitives::{FundingOutput, SpendingInput};
use crate::query::tx::TxQuery;
use crate::scripthash::FullHash;
use crate::store::{ReadStore, Row};
use bitcoincash::hash_types::Txid;

pub fn height_by_txid(store: &dyn ReadStore, txid: &Txid) -> Option<u32> {
    let key = HeightIndexRow::filter_by_txid(txid);
    let value = store.get(&key)?;
    Some(HeightIndexRow::from_row(&Row { key, value }).height())
}

/**
 * Collect all utxos sent to a scripthash.
 */
pub fn outputs_by_scripthash(store: &dyn ReadStore, scripthash: FullHash) -> Vec<OutputIndexRow> {
    let filter = ScriptHashIndexRow::filter_by_scripthash(scripthash);
    let scripthash_entries = store.scan(&filter).len();
    let utxos: Vec<OutputIndexRow> = store
        .scan(&filter)
        .iter()
        .flat_map(|scripthash: &Row| -> Vec<OutputIndexRow> {
            let scripthash = ScriptHashIndexRow::from_row(scripthash);
            store
                .scan(&OutputIndexRow::filter_by_outputhash(scripthash.outputhash))
                .iter()
                .map(OutputIndexRow::from_row)
                .collect()
        })
        .collect();
    trace!(
        "Found {} utxos, scripthash entries {}",
        utxos.len(),
        scripthash_entries
    );
    utxos
}

pub fn txheight_by_scripthash(store: &dyn ReadStore, scripthash: FullHash) -> Vec<HeightIndexRow> {
    outputs_by_scripthash(store, scripthash)
        .iter()
        .map(|output| output.txid())
        .map(|txid| {
            HeightIndexRow::from_row(
                store
                    .scan(&HeightIndexRow::filter_by_txid(&txid))
                    .last()
                    .unwrap(),
            )
        })
        .collect()
}

pub fn scripthash_to_fundingoutput(
    store: &dyn ReadStore,
    scripthash: FullHash,
    mempool: Option<&Tracker>,
) -> Vec<FundingOutput> {
    store
        .scan(&ScriptHashIndexRow::filter_by_scripthash(scripthash))
        .iter()
        .map(ScriptHashIndexRow::from_row)
        .flat_map(|scripthash| -> Vec<OutputIndexRow> {
            store
                .scan(&OutputIndexRow::filter_by_outputhash(scripthash.outputhash))
                .iter()
                .map(OutputIndexRow::from_row)
                .collect()
        })
        .map(|out| {
            let txid = out.txid();
            let height = height_by_txid(store, &txid);
            FundingOutput {
                txid,
                vout: out.index(),
                outpoint_hash: out.hash(),
                height: height.unwrap_or(0),
                value: out.value(),
                state: confirmation_state(mempool, &txid, height),
            }
        })
        .collect()
}

fn confirmation_state(
    mempool: Option<&Tracker>,
    txid: &Txid,
    height: Option<u32>,
) -> ConfirmationState {
    // If mempool parameter is not passed, this implies that it is known
    // that the transaction is confirmed.
    if mempool.is_none() || height.is_some() && height != Some(MEMPOOL_HEIGHT) {
        return ConfirmationState::Confirmed;
    }
    let mempool = mempool.unwrap();
    mempool.tx_confirmation_state(txid, height)
}

pub fn find_spending_input(
    store: &dyn ReadStore,
    funding: &FundingOutput,
    mempool: Option<&Tracker>,
) -> Option<SpendingInput> {
    let spender = store
        .scan(&InputIndexRow::filter_by_outputhash(&funding.outpoint_hash))
        .iter()
        .map(InputIndexRow::from_row)
        .last()?;

    let txid = spender.txid();
    let height = height_by_txid(store, &txid);

    Some(SpendingInput {
        txn_id: txid,
        height: height.unwrap_or_default(),
        funding_output_hash: funding.outpoint_hash,
        value: funding.value,
        state: confirmation_state(mempool, &txid, height),
    })
}

pub fn get_tx_spending_prevout(
    store: &dyn ReadStore,
    txquery: &TxQuery,
    prevout: &OutPoint,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_spending_output(store, prevout);
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, &txid);
    let tx = txquery.get(&txid, None, height)?;
    Ok(Some((
        tx,
        spender.index() as u32,
        height.unwrap_or_default(),
    )))
}

pub fn tx_spending_output(store: &dyn ReadStore, prevout: &OutPoint) -> Option<InputIndexRow> {
    let filter = InputIndexRow::filter_by_outputhash(&output_hash(prevout));
    store.scan(&filter).last().map(InputIndexRow::from_row)
}
