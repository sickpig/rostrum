#[cfg(feature = "nexa")]
use crate::nexa::hash_types::TxIdem;
use bitcoin_hashes::Hash;
use bitcoincash::consensus::encode::MAX_VEC_SIZE;
use bitcoincash::consensus::Decodable;
use bitcoincash::consensus::Encodable;
#[cfg(not(feature = "nexa"))]
use bitcoincash::Txid;
use bitcoincash::VarInt;

use crate::chaindef::OutPoint;
use crate::nexa::hash_types::OutPointHash;

#[inline]
pub fn encode_varint_u64(value: u64) -> Vec<u8> {
    let mut buff = unsigned_varint::encode::u64_buffer();
    let encoded = unsigned_varint::encode::u64(value, &mut buff);
    encoded.to_vec()
}

#[inline]
pub fn decode_varint_u64(index: &[u8]) -> u64 {
    unsigned_varint::decode::u64(index).unwrap().0
}

#[inline]
pub fn encode_varint_u32(value: u32) -> Vec<u8> {
    let mut buff = unsigned_varint::encode::u32_buffer();
    let encoded = unsigned_varint::encode::u32(value, &mut buff);
    encoded.to_vec()
}

#[inline]
pub fn decode_varint_u32(index: &[u8]) -> u32 {
    unsigned_varint::decode::u32(index).unwrap().0
}

#[inline]
pub fn encode_varint_usize(value: usize) -> Vec<u8> {
    let mut buff = unsigned_varint::encode::usize_buffer();
    let encoded = unsigned_varint::encode::usize(value, &mut buff);
    encoded.to_vec()
}

#[inline]
pub fn decode_varint_usize(index: &[u8]) -> usize {
    unsigned_varint::decode::usize(index).unwrap().0
}

#[cfg(not(feature = "nexa"))]
pub fn compute_output_hash(txid: &Txid, vout: u32) -> OutPointHash {
    let mut e = OutPointHash::engine();
    txid.consensus_encode(&mut e)
        .expect("failed to encode input txid");
    vout.consensus_encode(&mut e)
        .expect("failed to encode input index");
    OutPointHash::from_engine(e)
}

#[cfg(feature = "nexa")]
pub fn compute_output_hash(txidem: &TxIdem, n: u32) -> OutPointHash {
    let mut e = OutPointHash::engine();
    txidem
        .consensus_encode(&mut e)
        .expect("failed to encode input txid");
    n.consensus_encode(&mut e)
        .expect("failed to encode input index");
    OutPointHash::from_engine(e)
}

#[cfg(feature = "nexa")]
pub fn output_hash(previous_output: &OutPoint) -> OutPointHash {
    previous_output.hash
}

#[cfg(not(feature = "nexa"))]
pub fn output_hash(previous_output: &OutPoint) -> OutPointHash {
    compute_output_hash(&previous_output.txid, previous_output.vout)
}

pub fn decode_vector<D: std::io::Read, V: Decodable>(
    mut d: D,
) -> Result<Vec<V>, bitcoincash::consensus::encode::Error> {
    let vector_len = VarInt::consensus_decode(&mut d)?.0;
    let vector_bytes = (vector_len as usize)
        .checked_mul(std::mem::size_of::<V>())
        .ok_or(bitcoincash::consensus::encode::Error::ParseFailed(
            "Invalid length",
        ))?;
    if vector_bytes > MAX_VEC_SIZE {
        return Err(
            bitcoincash::consensus::encode::Error::OversizedVectorAllocation {
                requested: vector_bytes,
                max: MAX_VEC_SIZE,
            },
        );
    }
    let mut vector: Vec<V> = Vec::with_capacity(vector_len as usize);
    for _ in 0..vector_len {
        vector.push(Decodable::consensus_decode(&mut d)?);
    }
    Ok(vector)
}

pub fn encode_vector<S: std::io::Write, V: Encodable>(
    mut s: S,
    v: &[V],
) -> Result<usize, std::io::Error> {
    let mut len = 0;
    len += VarInt(v.len() as u64).consensus_encode(&mut s)?;
    for i in v {
        len += i.consensus_encode(&mut s)?;
    }
    Ok(len)
}
