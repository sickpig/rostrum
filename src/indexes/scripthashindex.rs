use std::convert::TryInto;

use crate::nexa::hash_types::OutPointHash;
use crate::store::Row;
use crate::util::Bytes;

#[derive(Serialize, Deserialize)]
pub struct ScriptHashIndexKey {
    pub code: u8,
    pub scripthash: [u8; 32],
}

#[derive(Serialize, Deserialize)]
pub struct ScriptHashIndexRow {
    key: ScriptHashIndexKey,
    pub outputhash: [u8; 32],
}

const SCRIPTHASH_INDEX_CODE: u8 = b'O';

impl ScriptHashIndexRow {
    pub fn new(scripthash: [u8; 32], outputhash: &OutPointHash) -> ScriptHashIndexRow {
        ScriptHashIndexRow {
            key: ScriptHashIndexKey {
                code: SCRIPTHASH_INDEX_CODE,
                scripthash,
            },
            outputhash: outputhash
                .to_vec()
                .try_into()
                .expect("failed to encode OutPointHash"),
        }
    }

    pub fn filter_by_scripthash(scripthash: [u8; 32]) -> Bytes {
        bincode::serialize(&ScriptHashIndexKey {
            code: SCRIPTHASH_INDEX_CODE,
            scripthash,
        })
        .unwrap()
    }

    pub fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self).unwrap(),
            value: vec![],
        }
    }

    pub fn from_row(row: &Row) -> ScriptHashIndexRow {
        bincode::deserialize(&row.key).expect("failed to read ScriptHashIndexRow")
    }
}
