use crate::encode::encode_varint_u32;
use crate::store::Row;
use crate::util::Bytes;
use bitcoincash::Txid;
use std::convert::TryInto;

use crate::encode::decode_varint_u32;

const SCRIPTHASH_INDEX_CODE: u8 = b'T';
#[derive(Serialize, Deserialize, Debug)]
pub struct HeightIndexKey {
    code: u8,
    pub txid: [u8; 32],
}

#[derive(Debug)]
pub struct HeightIndexRow {
    pub key: HeightIndexKey,
    height: Vec<u8>,
}

impl HeightIndexRow {
    pub fn new(txid: &Txid, height: u32) -> HeightIndexRow {
        HeightIndexRow {
            key: HeightIndexKey {
                code: b'T',
                txid: txid[..].try_into().unwrap(),
            },
            height: encode_varint_u32(height),
        }
    }

    pub fn filter_by_txid(txid: &Txid) -> Bytes {
        [vec![SCRIPTHASH_INDEX_CODE], txid.to_vec()].concat()
    }

    pub fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap(),
            value: bincode::serialize(&self.height).unwrap(),
        }
    }

    pub fn from_row(row: &Row) -> HeightIndexRow {
        HeightIndexRow {
            key: bincode::deserialize(&row.key).expect("failed to parse TxKey"),
            height: bincode::deserialize(&row.value).expect("failed to parse height"),
        }
    }

    pub fn txid(&self) -> Txid {
        use bitcoincash::hashes::Hash;
        Txid::from_slice(&self.key.txid).unwrap()
    }

    pub fn height(&self) -> u32 {
        decode_varint_u32(&self.height)
    }
}
