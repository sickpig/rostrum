#[cfg(feature = "nexa")]
pub type BlockHeader = crate::nexa::block::BlockHeader;
#[cfg(not(feature = "nexa"))]
pub type BlockHeader = bitcoincash::blockdata::block::BlockHeader;

#[cfg(feature = "nexa")]
pub type BlockHash = crate::nexa::hash_types::BlockHash;
#[cfg(not(feature = "nexa"))]
pub type BlockHash = bitcoincash::hash_types::BlockHash;

#[cfg(feature = "nexa")]
pub type Block = crate::nexa::block::Block;
#[cfg(not(feature = "nexa"))]
pub type Block = bitcoincash::blockdata::block::Block;

#[cfg(feature = "nexa")]
pub type OutPoint = crate::nexa::transaction::OutPoint;
#[cfg(not(feature = "nexa"))]
pub type OutPoint = bitcoincash::blockdata::transaction::OutPoint;

#[cfg(feature = "nexa")]
pub type TxIn = crate::nexa::transaction::TxIn;
#[cfg(not(feature = "nexa"))]
pub type TxIn = bitcoincash::blockdata::transaction::TxIn;

#[cfg(feature = "nexa")]
pub type TxOut = crate::nexa::transaction::TxOut;
#[cfg(not(feature = "nexa"))]
pub type TxOut = bitcoincash::blockdata::transaction::TxOut;

#[cfg(feature = "nexa")]
pub type Transaction = crate::nexa::transaction::Transaction;
#[cfg(not(feature = "nexa"))]
pub type Transaction = bitcoincash::blockdata::transaction::Transaction;
