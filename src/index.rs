use crate::cashaccount::CashAccountParser;
use crate::chaindef::Block;
use crate::chaindef::BlockHash;
use crate::chaindef::BlockHeader;
use crate::chaindef::{Transaction, TxIn};
use crate::daemon::Daemon;
use crate::encode::compute_output_hash;
use crate::errors::*;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::metrics::Metrics;
use crate::scripthash::{compute_script_hash, full_hash, FullHash};
use crate::signal::Waiter;
use crate::store::{ReadStore, Row, WriteStore};
use crate::util::{spawn_thread, HeaderEntry, HeaderList, HeaderMap, SyncChannel};
use bitcoincash::consensus::encode::{deserialize, serialize};
use std::collections::{HashMap, HashSet};
use std::sync::RwLock;

#[derive(Serialize, Deserialize)]
struct BlockKey {
    code: u8,
    hash: FullHash,
}

#[cfg(not(feature = "nexa"))]
#[inline]
fn is_coinbase_input(input: &TxIn) -> bool {
    use bitcoincash::Txid;
    let null_hash: Txid = Txid::default();
    input.previous_output.txid == null_hash
}
#[cfg(feature = "nexa")]
#[inline]
fn is_coinbase_input(_input: &TxIn) -> bool {
    // nexa does not have inputs in coinbase
    false
}
#[cfg(not(feature = "nexa"))]
#[inline]
fn get_output_hash(input: &TxIn) -> [u8; 32] {
    use std::convert::TryInto;
    compute_output_hash(&input.previous_output.txid, input.previous_output.vout)
        .to_vec()
        .try_into()
        .unwrap()
}
#[cfg(feature = "nexa")]
#[inline]
fn get_output_hash(input: &TxIn) -> [u8; 32] {
    use std::convert::TryInto;
    input.previous_output.hash.to_vec().try_into().unwrap()
}

#[cfg(feature = "nexa")]
fn index_outputs(tx: &Transaction) -> impl '_ + Iterator<Item = Row> {
    let txid = tx.txid();
    tx.output.iter().enumerate().map(move |(i, output)| {
        OutputIndexRow::new(
            &txid,
            &compute_output_hash(&tx.txidem(), i as u32),
            output.value,
            i as u32,
        )
        .to_row()
    })
}

#[cfg(not(feature = "nexa"))]
fn index_outputs(tx: &Transaction) -> impl '_ + Iterator<Item = Row> {
    let txid = tx.txid();
    tx.output.iter().enumerate().map(move |(i, output)| {
        OutputIndexRow::new(
            &txid,
            &compute_output_hash(&txid, i as u32),
            output.value,
            i as u32,
        )
        .to_row()
    })
}

#[cfg(feature = "nexa")]
fn index_scriptsig(tx: &Transaction) -> impl '_ + Iterator<Item = Row> {
    tx.output.iter().enumerate().map(move |(index, output)| {
        {
            ScriptHashIndexRow::new(
                compute_script_hash(output.script_pubkey.as_bytes()),
                &compute_output_hash(&tx.txidem(), index as u32),
            )
        }
        .to_row()
    })
}
#[cfg(not(feature = "nexa"))]
fn index_scriptsig(tx: &Transaction) -> impl '_ + Iterator<Item = Row> {
    tx.output.iter().enumerate().map(move |(index, output)| {
        {
            ScriptHashIndexRow::new(
                compute_script_hash(output.script_pubkey.as_bytes()),
                &compute_output_hash(&tx.txid(), index as u32),
            )
        }
        .to_row()
    })
}

pub fn index_transaction<'a>(
    txn: &'a Transaction,
    height: usize,
    cashaccount: Option<&CashAccountParser>,
) -> impl 'a + Iterator<Item = Row> {
    let txid = txn.txid();

    let inputs = txn
        .input
        .iter()
        .enumerate()
        .filter_map(move |(index, input)| {
            if is_coinbase_input(input) {
                None
            } else {
                let outputhash = get_output_hash(input);
                Some(InputIndexRow::new(outputhash, txid, index).to_row())
            }
        });
    let outputs = index_outputs(txn);

    let scriptsigs = index_scriptsig(txn);

    let cashaccount_row = match cashaccount {
        Some(cashaccount) => cashaccount.index_cashaccount(txn, height as u32),
        None => None,
    };
    // Persist transaction ID and confirmed height
    inputs
        .chain(outputs)
        .chain(scriptsigs)
        .chain(std::iter::once(
            HeightIndexRow::new(&txid, height as u32).to_row(),
        ))
        .chain(cashaccount_row)
}

pub fn index_block<'a>(
    block: &'a Block,
    height: usize,
    cashaccount: &'a CashAccountParser,
) -> impl 'a + Iterator<Item = Row> {
    let blockhash = block.block_hash();
    // Persist block hash and header
    let row = Row {
        key: bincode::serialize(&BlockKey {
            code: b'B',
            hash: full_hash(&blockhash[..]),
        })
        .unwrap(),
        value: serialize(&block.header),
    };
    block
        .txdata
        .iter()
        .flat_map(move |txn| index_transaction(txn, height, Some(cashaccount)))
        .chain(std::iter::once(row))
}

pub fn last_indexed_block(blockhash: &BlockHash) -> Row {
    // Store last indexed block (i.e. all previous blocks were indexed)
    Row {
        key: b"L".to_vec(),
        value: serialize(blockhash),
    }
}

pub fn read_indexed_blockhashes(store: &dyn ReadStore) -> HashSet<BlockHash> {
    let mut result = HashSet::new();
    for row in store.scan(b"B") {
        let key: BlockKey = bincode::deserialize(&row.key).unwrap();
        result.insert(deserialize(&key.hash).unwrap());
    }
    result
}

fn read_indexed_headers(store: &dyn ReadStore) -> HeaderList {
    let latest_blockhash: BlockHash = match store.get(b"L") {
        // latest blockheader persisted in the DB.
        Some(row) => deserialize(&row).unwrap(),
        None => BlockHash::default(),
    };
    trace!("latest indexed blockhash: {}", latest_blockhash);
    let mut map = HeaderMap::new();
    for row in store.scan(b"B") {
        let key: BlockKey = bincode::deserialize(&row.key).unwrap();
        let header: BlockHeader = deserialize(&row.value).unwrap();
        map.insert(deserialize(&key.hash).unwrap(), header);
    }
    let mut headers = vec![];
    let null_hash = BlockHash::default();
    let mut blockhash = latest_blockhash;
    while blockhash != null_hash {
        let header = map
            .remove(&blockhash)
            .unwrap_or_else(|| panic!("missing {} header in DB", blockhash));
        blockhash = header.prev_blockhash;
        headers.push(header);
    }
    headers.reverse();
    assert_eq!(
        headers
            .first()
            .map(|h| h.prev_blockhash)
            .unwrap_or(null_hash),
        null_hash
    );
    assert_eq!(
        headers
            .last()
            .map(BlockHeader::block_hash)
            .unwrap_or(null_hash),
        latest_blockhash
    );
    let mut result = HeaderList::empty();
    let entries = result.order(headers);
    result.apply(&entries, latest_blockhash);
    result
}

struct Stats {
    blocks: prometheus::IntCounter,
    txns: prometheus::IntCounter,
    vsize: prometheus::IntCounter,
    height: prometheus::IntGauge,
    duration: prometheus::HistogramVec,
}

impl Stats {
    fn new(metrics: &Metrics) -> Stats {
        Stats {
            blocks: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_blocks",
                "# of indexed blocks",
            )),
            txns: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_txns",
                "# of indexed transactions",
            )),
            vsize: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_vsize",
                "# of indexed vbytes",
            )),
            height: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_index_height",
                "Last indexed block's height",
            )),
            duration: metrics.histogram_vec(
                prometheus::HistogramOpts::new(
                    "rostrum_index_duration",
                    "indexing duration (in seconds)",
                ),
                &["step"],
            ),
        }
    }

    fn update(&self, block: &Block, height: usize) {
        self.blocks.inc();
        self.txns.inc_by(block.txdata.len() as u64);
        for tx in &block.txdata {
            self.vsize.inc_by(tx.size() as u64);
        }
        self.update_height(height);
    }

    fn update_height(&self, height: usize) {
        self.height.set(height as i64);
    }

    fn start_timer(&self, step: &str) -> prometheus::HistogramTimer {
        self.duration.with_label_values(&[step]).start_timer()
    }
}

pub struct Index {
    // TODO: store also latest snapshot.
    headers: RwLock<HeaderList>,
    daemon: Daemon,
    stats: Stats,
    batch_size: usize,
    cashaccount_activation_height: u32,
}

impl Index {
    pub fn load(
        store: &dyn ReadStore,
        daemon: &Daemon,
        metrics: &Metrics,
        batch_size: usize,
        cashaccount_activation_height: u32,
    ) -> Result<Index> {
        let stats = Stats::new(metrics);
        let headers = read_indexed_headers(store);
        stats.height.set((headers.len() as i64) - 1);
        Ok(Index {
            headers: RwLock::new(headers),
            daemon: daemon.reconnect()?,
            stats,
            batch_size,
            cashaccount_activation_height,
        })
    }

    pub fn reload(&self, store: &dyn ReadStore) {
        let mut headers = self.headers.write().unwrap();
        *headers = read_indexed_headers(store);
    }

    pub fn best_header(&self) -> Option<HeaderEntry> {
        let headers = self.headers.read().unwrap();
        headers.header_by_blockhash(&headers.tiphash()).cloned()
    }

    pub fn get_header(&self, height: usize) -> Option<HeaderEntry> {
        self.headers
            .read()
            .unwrap()
            .header_by_height(height)
            .cloned()
    }

    pub fn update(
        &self,
        store: &impl WriteStore,
        waiter: &Waiter,
    ) -> Result<(Vec<HeaderEntry>, HeaderEntry)> {
        let daemon = self.daemon.reconnect()?;
        let tip = daemon.getbestblockhash()?;
        let new_headers: Vec<HeaderEntry> = {
            let indexed_headers = self.headers.read().unwrap();
            indexed_headers.order(daemon.get_new_headers(&indexed_headers, &tip)?)
        };
        if let Some(latest_header) = new_headers.last() {
            info!("{:?} ({} left to index)", latest_header, new_headers.len());
        };
        let height_map: HashMap<BlockHash, usize> = new_headers
            .iter()
            .map(|h| (*h.hash(), h.height()))
            .collect();

        let chan = SyncChannel::new(self.batch_size);
        let sender = chan.sender();
        let blockhashes: Vec<BlockHash> = new_headers.iter().map(|h| *h.hash()).collect();
        let fetcher = spawn_thread("fetcher", move || {
            for blockhash in blockhashes.iter() {
                sender
                    .send(Some(daemon.getblock(blockhash)))
                    .expect("failed sending blocks to be indexed");
            }
            sender
                .send(None)
                .expect("failed sending explicit end of stream");
        });
        let cashaccount = CashAccountParser::new(Some(self.cashaccount_activation_height));

        let mut i = 0;
        let mut prev_blockhash = None;
        loop {
            waiter.poll()?;
            let timer = self.stats.start_timer("fetch");
            let block = chan
                .receiver()
                .recv()
                .expect("block fetch exited prematurely");
            timer.observe_duration();
            if block.is_none() {
                break;
            }
            let block = block.unwrap();
            let block = block?;

            let blockhash = block.block_hash();
            let height = *height_map
                .get(&blockhash)
                .unwrap_or_else(|| panic!("missing header for block {}", blockhash));

            let timer = self.stats.start_timer("index+write");
            i += 1;
            let indexed = index_block(&block, height, &cashaccount);
            if i % 1000 == 0 {
                // Occationally update the 'last indexed' marker. If indexing is
                // interrupted, it will restart at last marker.
                store.write(
                    indexed.chain(std::iter::once(last_indexed_block(&blockhash))),
                    false,
                );
            } else {
                store.write(indexed, false);
            };
            timer.observe_duration();
            self.stats.update(&block, height);
            prev_blockhash = Some(blockhash);
        }

        let timer = self.stats.start_timer("flush");
        if let Some(h) = prev_blockhash {
            store.write(std::iter::once(last_indexed_block(&h)), false);
        }
        store.flush(); // make sure no row is left behind
        timer.observe_duration();

        fetcher.join().expect("block fetcher failed");
        self.headers.write().unwrap().apply(&new_headers, tip);
        let tip_header = self
            .headers
            .read()
            .unwrap()
            .tip()
            .expect("failed to get tip header");
        assert_eq!(&tip, tip_header.hash());
        self.stats
            .update_height(self.headers.read().unwrap().len() - 1);
        Ok((new_headers, tip_header))
    }
}
