use std::io::Write;

use crate::encode::{decode_vector, encode_vector};
use crate::impl_nexa_consensus_encoding;
use crate::nexa::hash_types::OutPointHash;
use crate::nexa::hash_types::TxIdem;
use bitcoin_hashes::Hash;
use bitcoincash::consensus::Decodable;
use bitcoincash::consensus::Encodable;
use bitcoincash::Txid;
use bitcoincash::VarInt;
use byteorder::WriteBytesExt;

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub struct OutPoint {
    pub hash: OutPointHash,
}

impl OutPoint {
    pub fn new(tx_idem: TxIdem, out_index: u32) -> OutPoint {
        let mut e = OutPointHash::engine();
        tx_idem
            .consensus_encode(&mut e)
            .expect("failed to encode txidem");
        out_index
            .consensus_encode(&mut e)
            .expect("failed to encode output_index");

        OutPoint {
            hash: OutPointHash::from_engine(e),
        }
    }
    pub fn is_null(&self) -> bool {
        self.hash.eq(&OutPointHash::default())
    }
}
impl_nexa_consensus_encoding!(OutPoint, hash);

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TxIn {
    pub txin_type: u8,
    pub previous_output: OutPoint,
    pub script_sig: bitcoincash::Script,
    pub sequence: u32,
    pub amount: u64,
}

impl_nexa_consensus_encoding!(
    TxIn,
    txin_type,
    previous_output,
    script_sig,
    sequence,
    amount
);

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TxOut {
    pub txout_type: u8,
    pub value: u64,
    pub script_pubkey: bitcoincash::Script,
}
impl_nexa_consensus_encoding!(TxOut, txout_type, value, script_pubkey);

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(crate = "actual_serde"))]
pub struct Transaction {
    pub version: u8,
    pub lock_time: u32,
    pub input: Vec<TxIn>,
    pub output: Vec<TxOut>,
}

impl Transaction {
    pub fn txidem(&self) -> TxIdem {
        let mut s = TxIdem::engine();
        self.version.consensus_encode(&mut s).unwrap();

        // We need to encode inputs without scriptsig, so not using `encode_vector`.
        VarInt(self.input.len() as u64)
            .consensus_encode(&mut s)
            .unwrap();
        for i in &self.input {
            i.txin_type.consensus_encode(&mut s).unwrap();
            i.previous_output.consensus_encode(&mut s).unwrap();
            i.sequence.consensus_encode(&mut s).unwrap();
            i.amount.consensus_encode(&mut s).unwrap();
        }
        encode_vector(&mut s, &self.output).unwrap();
        self.lock_time.consensus_encode(&mut s).unwrap();

        TxIdem::from_engine(s)
    }

    pub fn txid(&self) -> Txid {
        use bitcoin_hashes::sha256d;
        let mut satisfier_script_hash = sha256d::Hash::engine();
        i32::consensus_encode(&(self.input.len() as i32), &mut satisfier_script_hash).unwrap();

        for i in &self.input {
            satisfier_script_hash
                .write_all(i.script_sig.as_bytes())
                .unwrap();
            satisfier_script_hash.write_u8(0xff).unwrap();
        }

        let mut txid = Txid::engine();
        let txidem = self.txidem();
        let satisfier = sha256d::Hash::from_engine(satisfier_script_hash);
        txidem.consensus_encode(&mut txid).unwrap();
        satisfier.consensus_encode(&mut txid).unwrap();

        Txid::from_engine(txid)
    }

    /// Returns the regular byte-wise consensus-serialized size of this transaction.
    pub fn size(&self) -> usize {
        let mut input_size = 0;

        for input in &self.input {
            input_size += 32 + 4 + 4 + /* outpoint (32+4) + nSequence */ VarInt(input.script_sig.len() as u64).len() + input.script_sig.len();
        }
        let mut output_size = 0;
        for output in &self.output {
            output_size += 8 + /* value */ VarInt(output.script_pubkey.len() as u64).len() + output.script_pubkey.len();
        }
        let non_input_size =
        // version:
        4 +
        // count varints:
        VarInt(self.input.len() as u64).len() +
        VarInt(self.output.len() as u64).len() +
        output_size +
        // lock_time
        4;

        non_input_size + input_size
    }

    pub fn is_coin_base(&self) -> bool {
        self.input.is_empty()
    }
}

impl Encodable for Transaction {
    fn consensus_encode<S: std::io::Write>(&self, mut s: S) -> Result<usize, std::io::Error> {
        let mut len = 0;
        len += self.version.consensus_encode(&mut s)?;

        // Can't implement Encodable trait for Vec<TxIn> or TxOut because of Rust orphan rules.
        // (We don't own Encodeable and we don't own Vec).
        len += encode_vector(&mut s, &self.input)?;
        len += encode_vector(&mut s, &self.output)?;

        len += self.lock_time.consensus_encode(s)?;
        Ok(len)
    }
}

impl Decodable for Transaction {
    fn consensus_decode<D: std::io::Read>(
        d: D,
    ) -> Result<Self, bitcoincash::consensus::encode::Error> {
        let mut d = d.take(bitcoincash::consensus::encode::MAX_VEC_SIZE as u64);
        let version = u8::consensus_decode(&mut d).unwrap();

        let input: Vec<TxIn> = decode_vector(&mut d).unwrap();
        let output: Vec<TxOut> = decode_vector(&mut d).unwrap();

        Ok(Transaction {
            version,
            input,
            output,
            lock_time: Decodable::consensus_decode(d).unwrap(),
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use bitcoincash::{
        consensus::encode::deserialize, consensus::encode::deserialize_partial,
        consensus::encode::serialize,
    };

    #[test]
    /** Coinbase transaction `8b81d5de14b7b0951a2981c2d58919e9878847af1fad7c4e0fa17957ed8216b6` */
    fn deserialize_coinbase_transaction() {
        let tx_hex = "0000020100ca9a3b0000000017005114a64ba51750cadde003c7e8cfc400959d7ec7e0420000000000000000000f6a03ba8e000008000000000000000000000000";
        let serialized = hex::decode(&tx_hex).unwrap();
        let tx: (Transaction, usize) = deserialize_partial(&serialized).unwrap();
        assert_eq!(tx.1, serialized.len());
        let tx = tx.0;
        assert!(tx.is_coin_base());
        assert_eq!(tx.output[0].value, 1000000000);
        assert_eq!(tx.output[1].value, 0);
    }

    #[test]
    /** Coinbase transaction `8b81d5de14b7b0951a2981c2d58919e9878847af1fad7c4e0fa17957ed8216b6` */
    fn serialize_coinbase_transaction() {
        let expected_tx_hex = "0000020100ca9a3b0000000017005114a64ba51750cadde003c7e8cfc400959d7ec7e0420000000000000000000f6a03ba8e000008000000000000000000000000";
        let tx: Transaction = deserialize(&hex::decode(&expected_tx_hex).unwrap()).unwrap();
        assert_eq!(hex::encode(&serialize(&tx)), expected_tx_hex)
    }

    #[test]
    fn txid_coinbase_transaction() {
        let tx_hex = "0000020100ca9a3b0000000017005114a64ba51750cadde003c7e8cfc400959d7ec7e0420000000000000000000f6a03ba8e000008000000000000000000000000";
        let tx: Transaction = deserialize(&hex::decode(&tx_hex).unwrap()).unwrap();
        assert_eq!(
            tx.txidem().to_string(),
            "79bd936e54853c39bd00e6922a6c050f123ceb9cf3d51cabf806f6a5a13afc9a"
        );
        assert_eq!(
            tx.txid().to_string(),
            "8b81d5de14b7b0951a2981c2d58919e9878847af1fad7c4e0fa17957ed8216b6"
        );
    }
}
