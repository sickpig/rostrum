use crate::impl_nexa_hashencode;
use bitcoin_hashes::sha256;
use bitcoin_hashes::sha256d;
use bitcoincash::hashes::Hash;

hash_newtype!(BlockHash, sha256::Hash, 32, doc = "A nexa block hash.");
hash_newtype!(
    TxIdem,
    sha256d::Hash,
    32,
    doc = "A bitcoin transaction hash/transaction idem."
);
hash_newtype!(
    OutPointHash,
    sha256::Hash,
    32,
    doc = "Hash pointing to a outpoint (utxo)"
);
impl_nexa_hashencode!(BlockHash);
impl_nexa_hashencode!(TxIdem);
impl_nexa_hashencode!(OutPointHash);
