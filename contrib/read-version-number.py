#!/usr/bin/env python3
"""
Script that outputs version from Cargo.toml
"""
import toml
import os
script_path = os.path.dirname(os.path.realpath(__file__))
cfg_path = os.path.join(script_path,
        "..", "Cargo.toml"
)

with open(cfg_path, 'r') as fh:
    config = toml.loads(fh.read())

print(config['package']['version'])
