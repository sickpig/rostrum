# Rostrum - Electrum Server in Rust

An efficient implementation of Electrum Server.

![logo](https://gitlab.com/bitcoinunlimited/rostrum/-/raw/master/contrib/images/logo-platform-256x256.png)

**[Documentation](https://bitcoinunlimited.gitlab.io/rostrum/)**

Rostrum is an efficient implementation of Electrum Server and can be used
as a drop-in replacement for ElectrumX. In addition to the TCP RPC interface,
it also provides WebSocket support.

Rostrum fully implements the
[v1.4.3 Electrum Cash protocol](https://bitcoincash.network/electrum/)
and in addition to [useful extensions](https://bitcoinunlimited.gitlab.io/rostrum/usage/), including CashAccounts.

The server indexes the entire Bitcoin Cash blockchain, and the resulting index
enables fast queries for blockchain applications and any given user wallet,
allowing the user to keep real-time track of his balances and his transaction
history.

When run on the user's own machine, there is no need for the wallet to
communicate with external Electrum servers,
thus preserving the privacy of the user's addresses and balances.

## Features

- Supports Electrum protocol [v1.4.3](https://bitcoincash.network/electrum/)
- Maintains an index over transaction inputs and outputs, allowing fast balance
  queries.
- Fast synchronization of the Bitcoin Cash blockchain on modest hardware
- Low index storage overhead (~20%), relying on a local full node for
  transaction retrieval.
- `txindex` is not required for the Bitcoin node, however it does improve
  performance.
- Uses a single [RocksDB](https://github.com/spacejam/rust-rocksdb) database
  for better consistency and crash recovery.

### Notable features unique to Rostrum

- Has [really good integration with Bitcoin Unlimited](https://github.com/BitcoinUnlimited/BitcoinUnlimited/blob/release/doc/bu-electrum-integration.md).
- Supports all Bitcoin Cash full nodes that have basic `bitcoind` RPC support.
- The best integration test coverage of all electrum server implementations.
  (see Tests section)
- [CashAccount support](https://honest.cash/v2/dagur/fast-cashaccount-lookups-using-bitbox-and-electrum-4781)

## Usage

See [here](https://bitcoinunlimited.gitlab.io/rostrum/usage/) for installation, build and usage instructions.

## Tests

Run unit tests with `cargo test`.

Rostrum has good integration test coverage with Bitcoin Unlimited full node. See the [testing documentation](
https://bitcoinunlimited.gitlab.io/rostrum/tests/)

## Linters

Code linters and formatting are checked with continuous integration before accepting chanages. When contributing, please run `cargo clippy` to catch common mistakes and
improve your code, as well as `cargo fmt` to format the code.
