# Building binaries

Install a recent version of Rust. A good tool to install Rust is [rustup](https://rustup.rs/).

## Other dependencies
Also the packages `clang` and `cmake` are required to build the database used, rocksdb.

### Debian
```bash
$ sudo apt update
$ sudo apt install clang cmake build-essential # for building 'rust-rocksdb'
```

### OS X

When using [brew](https://brew.sh/).

```bash
$ brew install cmake
```

## First build

To clone and build release optimized binary:

```bash
$ git clone https://gitlab.com/bitcoinunlimited/rostrum.git
$ cd rostrum
$ cargo build --release
```

## Docker
```bash
$ docker build -t rostrum-app .
$ docker run --network host \
             --volume $HOME/.bitcoin:/home/user/.bitcoin:ro \
             --volume $PWD:/home/user \
             --rm -i -t rostrum-app \
             rostrum -vvvv --timestamp --db-dir /home/user/db
```