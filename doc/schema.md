# Index Schema

The index is stored at a single RocksDB database using the following schema:

## Transaction output index

Allows efficiently finding transaction funding a specific output:

|  Code  | Output Hash             |   |  Funding TxID      | Funding Output Index | Funding value  |
| ------ | ----------------------  | - | ------------------ | -------------------- | -------------- |
| `b'H'` | `outputhash` (32 bytes) |   | `txid` (32 bytes)  | `varint`             | `varint`       |

## ScriptSig to output index

Allows efficiently finding all funding transactions for a specific address:

|  Code  | Script Hash Prefix   |  Funding output hash |   |
| ------ | -------------------- |  ------------------- | - |
| `b'O'` | `SHA256(script)`     | `outputhash`         |   |

## Transaction input index

Allows efficiently finding spending transaction of a specific output:

|  Code  | Output Hash               |   | Spending TxID         | Input index |
| ------ | ------------------------- | - | --------------------- | ----------- |
| `b'I'` | `output hash` (32 bytes)  |   | `txid` (32 bytes)     | `varint`    |

## TxID to confirmation height index

In order to save storage space, we store the full transaction IDs once, and use their 8-byte prefixes for the indexes above.

|  Code  | Transaction ID    |   | Confirmed height   |
| ------ | ----------------- | - | ------------------ |
| `b'T'` | `txid` (32 bytes) |   | `varint`           |

Note that this mapping allows us to use `getrawtransaction` RPC to retrieve actual transaction data from without `-txindex` enabled
(by explicitly specifying the [blockhash](https://github.com/bitcoin/bitcoin/commit/497d0e014cc79d46531d570e74e4aeae72db602d)).

## CashAccount index

Allows finding all transactions containing CashAccount registration by name and block height.

|  Code  | Account name              | Registration TxID Prefix   |   |
| ------ | ------------------------- | -------------------------- | - |
| `b'C'` | `SHA256(name#height)`     | `txid`                     |   |
