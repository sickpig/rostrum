
# Usage

For installation, see [Install](install.md) or [build from source](build.md).

For configuration, see [configuration](configuration.md) and [how to connect with full node](bitcoind.md).

For connecting to rostrum with wallet or a programming library, see [connecting article](connecting.md).

For public server, or websocket support, you'll want to add [SSL Support](ssl.md).

Rostrum serves loads of metrics for monitoring, see [monitoring](monitoring.md).
